import "./App.css";
import { useState, useEffect, React } from "react";
import { ThemeContext } from "./components/ThemeContext";
import { Routes, Route, Link } from "react-router-dom";
import ProductList from "./components/ProductList";
import Navbar from "./components/Navbar";
import Cart from "./components/Cart";
import EachProduct from "./components/EachProduct";
import AddAProduct from "./components/AddAProduct";
import EditTheProduct from "./components/EditTheProduct";
import DeleteTheProduct from "./components/DeleteTheProduct";
import { API_STATES } from "./components/API_STATES";
import axios from "axios";

const api = axios.create({
  baseURL: `https://fakestoreapi.com/products`,
});

function App() {
  const [theme, setTheme] = useState("light");
  const [cart, setCart] = useState([]);
  const [products, setProducts] = useState({
    productsList: [],
    status: API_STATES.LOADING,
  });

  useEffect(() => {
    api
      .get("/")
      .then((response) => {
        return setProducts((prev) => {
          return {
            ...prev,
            productsList: response.data,
            status: API_STATES.LOADED,
          };
        });
      })
      .catch((err) => {
        return setProducts((prev) => {
          return { ...prev, status: API_STATES.ERROR };
        });
      });
  },[]);

  return (
    <div className="App">
      <Navbar theme={theme} setTheme={setTheme} cart={cart} />
      <Link to="/addAProduct">
        <button className="add-a-product">Add a product</button>
      </Link>
      <ThemeContext.Provider value={theme}>
        <Routes>
          <Route
            path="/"
            element={
              <ProductList products={products} cart={cart} setCart={setCart} />
            }
          />
          <Route
            path="/cart"
            element={<Cart cart={cart} setCart={setCart} products={products} />}
          />
          <Route
            path="/addAProduct"
            element={<AddAProduct setProducts={setProducts} />}
          />
          <Route
            path="/editTheProduct/:productid"
            element={
              <EditTheProduct products={products} setProducts={setProducts} setCart={setCart}/>
            }
          />
          <Route
            path="/deleteTheProduct/:productid"
            element={<DeleteTheProduct setProducts={setProducts} setCart={setCart} />}
          />
          <Route
            path="/:productId"
            element={<EachProduct products={products} setCart={setCart} />}
          />
        </Routes>
      </ThemeContext.Provider>
    </div>
  );
}

export default App;
