import React from "react";
import "./style.css";
import { useContext } from "react";
import { ThemeContext } from "./ThemeContext";
import { Link } from "react-router-dom";

function ProductTile(props) {
  const theme = useContext(ThemeContext);
  const bg = "card bg-" + theme;
  const { product, setCart } = props;

  function addItemToCart(setCart, product) {
    setCart((prevCart) => {
      if (prevCart.length === 0) {
        return [{ ...product, quantity: 1 }];
      } else {
        let found = false;
        let oldProduct = prevCart.map((cartItem) => {
          if (cartItem.id === product.id) {
            found = true;
            return { ...cartItem, quantity: cartItem.quantity + 1 };
          } else {
            return cartItem;
          }
        });
        if (found) {
          return oldProduct;
        } else {
          return [...prevCart, { ...product, quantity: 1 }];
        }
      }
    });
  }

  return (
    <>
      <div className={bg}>
        <div className="edit-and-delete">
          <Link to={`/editTheProduct/${product.id}`}>
            <i className="fa-solid fa-pen-to-square edit-btn"></i>
          </Link>
          <Link to={`/deleteTheProduct/${product.id}`}>
            <i className="fa-solid fa-trash delete-btn"></i>
          </Link>
        </div>

        <Link className="product-info" to={`/${product.id}`}>
          <img className="product-image" src={product.image} alt="img" />
          <div className="product-title">
            <b>{product.title}</b>
          </div>
          <div className="price">${product.price}</div>
          <div className="rating">
            <i className="fa-solid fa-star star"></i>
            <span className="rate">{product.rating.rate}</span>
            <span className="count">({product.rating.count})</span>
          </div>
        </Link>
        <div className="buttons">
          <button
            className="add-to-cart"
            onClick={() => addItemToCart(setCart, product)}
          >
            Add to cart
          </button>
          <button className="buy-now">Buy Now</button>
        </div>
      </div>
    </>
  );
}

export default ProductTile;

// addItemToCart(setCart,product)
