import React from "react";
import { useParams, Link, useNavigate } from "react-router-dom";

function DeleteTheProduct(props) {
  const { setProducts, setCart } = props;
  const productId = useParams();

  let answer = "";

  function handleChange(event) {
    answer = event.target.value;
  }

  const navigate = useNavigate();

  function handleSubmit(event) {
    event.preventDefault();
    if (answer == "yes") {
      setProducts((prevProducts) => {
        let newproductsList = prevProducts.productsList.filter((each) => {
          if (each.id != productId.productid) return each;
        });
        return { ...prevProducts, productsList: newproductsList };
      });
      setCart((prevCart) =>{
        return prevCart.filter((each) =>{ 
            if(each.id != productId.productid){
                return each;
            }
        } )
      })
      navigate("/");
    }
  }
  return (
    <>
      <Link to="/"><button className="back-btn">back</button></Link>
      <div className="products-details-list">
        <form onSubmit={handleSubmit}>
          <label htmlFor="">Are you sure you want to delete the product?</label>
          <input
            type="text"
            className="input-bar"
            onChange={handleChange}
            name=""
            id=""
          />
          <button type="submit" className="submit-btn">
            submit
          </button>
        </form>
        <p>type yes to continue</p>
      </div>
    </>
  );
}

export default DeleteTheProduct;
