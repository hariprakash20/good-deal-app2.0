import React, { useState } from "react";
import { Link, useParams, useNavigate } from "react-router-dom";
import { API_STATES } from "./API_STATES";

function EditTheProduct(props) {
  const productId = useParams();
  const { products, setProducts, setCart } = props;

  const product = products.productsList.filter(
    (each) => each.id == productId.productid
  )[0];

  const [editProduct, setEditProduct] = useState({});

  function checkAvailability(){
    setCart((prevCart) =>{
        const cartProduct = prevCart.filter(
            (each) => each.id == productId.productid
          )[0];

          if(!cartProduct){
            return [...prevCart];
          }
        let nonEditedProducts = prevCart.filter((each) => {
            if (each.id != productId.productid) return each;
          });
        return  [...nonEditedProducts, { ...cartProduct, ...editProduct }];
    })
  }

  function handleChange(event) {
    setEditProduct((prevProduct) => {
      return { ...prevProduct, [event.target.name]: event.target.value };
    });
  }

  const navigate = useNavigate();

  function handleSubmit(event) {
    event.preventDefault();

    setProducts((prevProducts) => {
      let nonEditedProducts = prevProducts.productsList.filter((each) => {
        if (each.id != productId.productid) return each;
      });
      return {
        productsList: [...nonEditedProducts, { ...product, ...editProduct }],
        status: API_STATES.LOADED,
      };
    });
      checkAvailability();


    navigate("/");
  }

  return (
    <>
      <Link to="/">
        <button className="back-btn">back</button>
      </Link>
      <div className="product-details-list">
        {products.status === API_STATES.LOADING ? (
          <div className="loader"></div>
        ) : products.status === API_STATES.ERROR ? (
          <h1>Error occured in Fetching the data</h1>
        ) : product ? (
          <>
            <form onSubmit={handleSubmit}>
              <div className="input">
                <label htmlFor="">title :</label>
                <input
                  className="input-bar"
                  type="text"
                  name="title"
                  onChange={handleChange}
                  defaultValue={product.title}
                  required
                />
              </div>
              <div className="input">
                <label htmlFor="">price :</label>
                <input
                  className="input-bar"
                  type="number"
                  step="0.01"
                  name="price"
                  onChange={handleChange}
                  defaultValue={product.price}
                  required
                />
              </div>
              <div className="input">
                <label htmlFor="">description :</label>
                <input
                  className="input-bar"
                  type="text"
                  name="description"
                  onChange={handleChange}
                  defaultValue={product.description}
                  required
                />
              </div>
              <div className="input">
                <label htmlFor="">category :</label>
                <input
                  className="input-bar"
                  type="text"
                  name="category"
                  onChange={handleChange}
                  defaultValue={product.category}
                  required
                />
              </div>
              <button type="submit" className="submit-btn">
                submit
              </button>
            </form>
          </>
        ) : (
          <h1>No products to display</h1>
        )}
      </div>
    </>
  );
}

export default EditTheProduct;
