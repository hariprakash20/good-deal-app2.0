import React from "react";
import { Link } from "react-router-dom";
import CartItem from "./CartItem";

function Cart(props) {
  const { cart, setCart, products } = props;
  let total = 0;

  return (
    <>
      <Link to="/">
        <button className="btn-back">back</button>
      </Link>

      {cart.length ? (
        <div className="all-cart-items">
          {cart.map((item) => {
            total = total + item.price * item.quantity;
            return <CartItem item={item} key={item.id} setCart={setCart} />;
          })}
          <div className="total">
            <h1>Grand Total:{total.toFixed(2)}</h1>
          </div>
        </div>
      ) : (
        <h1>no items in cart</h1>
      )}
    </>
  );
}

export default Cart;
