import React from "react";

function CartItem(props) {
  const { item, setCart } = props;
  function handleQuantity(event, id){
        setCart((prevCart) => {
            let alteredCart = prevCart.map(cartItem =>{
                if(cartItem.id === id){
                    if(event.target.outerText === '+'){
                        return {...cartItem, quantity: cartItem.quantity+1};  
                    }
                    else{
                        if(cartItem.quantity>1)
                        return {...cartItem, quantity: cartItem.quantity-1}; 
                    }
                  }
                  else{
                    return cartItem;
                  }
            })
            return alteredCart.filter(cartItem =>{
                if(cartItem !== undefined)
                return cartItem
            })

        })
  }
  return (
    <div className="cart-item">
      <div className="cart-image-div">
        <img className="image" src={item.image} alt="" />
      </div>
      <div className="product-details">
        <h2>{item.title}</h2>
      </div>
      <div className="billing">
        <h2>${item.price}</h2>
        <h2>
          Quantity: 
          <button className="quantity-change" onClick={(event) =>handleQuantity(event , item.id)}> + </button>
          {item.quantity}
          <button className="quantity-change" onClick={(event) =>handleQuantity(event, item.id)}> - </button>
        </h2>
        <h2>Sub-Total: {(item.price * item.quantity).toFixed(2)}</h2>
      </div>
    </div>
  );
}

export default CartItem;
