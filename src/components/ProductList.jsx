import React from 'react'
import './style.css'
import ProductTile from './ProductTile'
import { API_STATES } from './API_STATES'

function ProductList(props) {
    const {products, setCart} = props
    
  return (
    <>
        <div className="products-list">
        {products.status === API_STATES.LOADING ? (
          <div className="loader"></div>
        ) : products.status === API_STATES.ERROR ? (
          <h1>Error occured in Fetching the data</h1>
        ) : products.productsList ? (
          products.productsList.map((product) => (
            <ProductTile product={product} key={product.id} />
          ))
        ) : (
          <h1>No products to display</h1>
        )}
        </div>
      </>
  )
}

export default ProductList