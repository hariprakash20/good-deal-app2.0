import React from "react";
import "./style.css";
import { Link } from "react-router-dom";

function Navbar(props) {
  const { theme, setTheme, cart } = props;

  let itemCount = cart.reduce((accumlator, item)=> {
    return accumlator+item.quantity
  },0)

  return (
    <div className="navbar">
      <div className="logo">
        <i className="fa-regular fa-handshake"></i>Good deal
      </div>
      <div className="search">
        <input type="text" />
        <button>
          <i className="fa-solid fa-magnifying-glass"></i>
        </button>
      </div>
      <div className="nav-options">
      <div className="categories">
        <select name="categories" id="">
          <option value="">Categories</option>
        </select>
      </div>

      <button
        className={`mode bg-${theme}`}
        onClick={() => setTheme(theme === "dark" ? "light" : "dark")}
      >
        mode
      </button>
      <Link to="/cart">
        <div className="cart">
          <i className="fa-solid fa-cart-shopping"></i>
          <button className="badge">{itemCount}</button>
        </div>
      </Link>
        
      </div>
    </div>
  );
}

export default Navbar;
