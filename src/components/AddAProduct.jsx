import { React } from "react";
import { Link, useNavigate } from "react-router-dom";
import { useState } from "react";
import { v4 as uuidv4 } from "uuid";
import { API_STATES } from './API_STATES'

function AddAProduct(props) {
  const { setProducts } = props;
  const [newProduct, setNewProduct] = useState({
    id: uuidv4(),
    image:
      "https://user-images.githubusercontent.com/43302778/106805462-7a908400-6645-11eb-958f-cd72b74a17b3.jpg",
    rating: {
      rate: 0,
      count: 0,
    },
  });

  function handleChange(event) {
    setNewProduct((prevProduct) => {
      return { ...prevProduct, [event.target.name]: event.target.value };
    });
  }

  const navigate = useNavigate();

  function handleSubmit(event) {
    event.preventDefault();
    setProducts((prevProducts) => {
      return {
        productsList: [...prevProducts.productsList, newProduct],
        status: API_STATES.LOADED,
      };
    });
    navigate('/');

  }
  return (
    <>
      <Link to="/"><button className="back-btn">back</button></Link>
      <div>AddAProduct</div>
      <div className="products-details-list">
        <form onSubmit={handleSubmit}>
          <div className="input">
            <label htmlFor="">title :</label>
            <input type="text" name="title" className="input-bar" onChange={handleChange} required />
          </div>
          <div className="input">
            <label htmlFor="">price :</label>
            <input
              type="number"
              name="price"
              step="0.01"
              className="input-bar"
              onChange={handleChange}
              required
            />
          </div>
          <div className="input">
            <label htmlFor="">description :</label>
            <input
              type="text"
              name="description"
              className="input-bar"
              onChange={handleChange}
              required
            />
          </div>
          <div className="input">
            <label htmlFor="">category :</label>
            <input
              type="text"
              name="category"
              className="input-bar"
              onChange={handleChange}
              required
            />
          </div>
          <button className="submit-btn" type="submit">submit</button>
        </form>
      </div>
    </>
  );
}

export default AddAProduct;
