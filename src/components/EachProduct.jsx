import React from "react";
import { Link, useParams } from "react-router-dom";
import { API_STATES } from "./API_STATES"

function EachProduct(props) {
  const { productId } = useParams();
  const { products, setCart } = props;
  
  function addItemToCart(setCart, product) {
    setCart((prevCart) => {
      if (prevCart.length === 0) {
        return [{ ...product, quantity: 1 }];
      } else {
        let found = false;
        let oldProduct = prevCart.map((cartItem) => {
          if (cartItem.id === product.id) {
            found = true;
            return { ...cartItem, quantity: cartItem.quantity + 1 };
          } else {
            return cartItem;
          }
        });
        if (found) {
          return oldProduct;
        } else {
          return [...prevCart, { ...product, quantity: 1 }];
        }
      }
    });
  }
  const product = products.productsList.filter((each) => each.id == productId)[0];
  return (
    <>
      <Link to="/">
        <button className="btn-back">back</button>
      </Link>
      {products.status === API_STATES.LOADING ? (
        <div className="loader"></div>
      ) : products.status === API_STATES.ERROR ? (
        <h1>Error occured in Fetching the data</h1>
      ) : product ? (
        <div className="each-product">
          <img className="each-product-image" src={product.image} alt="" />
          <div className="each-product-details">
            <div className="title">
              <h2>{product.title}</h2>
            </div>
            <div className="description">
              <h4>{product.description}</h4>
            </div>
            <div className="rating">
              <i className="fa-solid fa-star star"></i>
              {product.rating.rate}({product.rating.count})
            </div>
            <div className="buttons">
              <button
                className="add-to-cart"
                onClick={() => addItemToCart(setCart, product)}
              >
                Add to cart
              </button>
              <button className="buy-now">Buy Now</button>
            </div>
          </div>
        </div>
      ) : (
        <h1>products Not available</h1>
      )}
    </>
  );
}

export default EachProduct;
